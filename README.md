# Github Pages Auto Deployer

Use git lab private project ci pipeline to deploy your hexo to github pages.

With git lab ci, we can deploy Hexo to Github pages automatically when there is changes pushed to your repo. 

## How to use?
- Create a private repo in your gitlab
- Add a private ssh key to the project
- Add public ssh key to your github
- Add .gitlab-ci.yml to your project
- Push your changes and see the result :)

